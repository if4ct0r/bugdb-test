var app = angular.module('bugubali', []);

app.controller('BugSearch', ['$scope', '$http', function($scope, $http) {
	this.query = {
			user: ''
	};
	$scope.bugResults = [{rptno: 'N/A',
		 subject: 'No data yet'}];
	
	this.submitquery = function(){
		$scope.loading = true;
		console.log('Attempting to search with the following parameters: '+this.query.user);
		$http.get('/bugs/updatedby/'+this.query.user).success(function(data){
					$scope.bugResults = data.bugSearchResults; 
				},
				function(data){
						//TODO: handle error
		}).finally(function(){
			$scope.loading = false;
		});
	};
        
}]);