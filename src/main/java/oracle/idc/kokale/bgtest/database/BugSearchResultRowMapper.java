package oracle.idc.kokale.bgtest.database;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import oracle.idc.kokale.bgtest.beans.BugSearchResult;

public class BugSearchResultRowMapper<T> implements RowMapper<T>{

	@Override
	public T mapRow(ResultSet rset, int row) throws SQLException {
		BugSearchResult bsres = new BugSearchResult();
		bsres.setRptno(rset.getInt(BugSearchDao.COL_IDX_RPTNO));
		bsres.setSubject(rset.getString(BugSearchDao.COL_IDX_SUBJECT));
		
		return (T) bsres;
	}

}

