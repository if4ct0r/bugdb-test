package oracle.idc.kokale.bgtest.database;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import oracle.idc.kokale.bgtest.beans.BugSearchResult;

@Repository
public class BugSearchDao {
	
	public static final int COL_IDX_RPTNO = 1;
	public static final int COL_IDX_SUBJECT = 2;
	private static final Logger logger = LoggerFactory.getLogger(BugSearchDao.class);
	
	@Autowired
	JdbcTemplate jdbcTemplate;
	
	public void setTemplate(JdbcTemplate jdbcTemplate) {
		this.jdbcTemplate = jdbcTemplate;
	}
	
	String sql_user_comments_no_filter_join = "select distinct(bbody.rptno) "
			+ ", bhead.subject "
			+ " from rptbody bbody join rpthead bhead "
			+ " on bbody.rptno = bhead.rptno "
			+ " where bbody.upd_by = '?'";
	public List<BugSearchResult> queryUserComments(String guid) {	
		//TODO: Better way to bind the values
		String sql_prepd = sql_user_comments_no_filter_join.replace("?", guid.toUpperCase());
		logger.info("Executing query: "+sql_prepd);
		
		return jdbcTemplate.query(sql_prepd, new BugSearchResultRowMapper<BugSearchResult>());
		
	}
	
	String sql_user_comments = "select distinct(bhead.rptno), bhead.subject "+
			"from RPTHEAD bhead join RPTBODY bbody "+
			"on bhead.rptno = bbody.rptno "+
			"and bbody.upd_by = '?' ";
	// Note: for now, protocol for filter conditions
	// filter=prod:<product_id>|days:<days>|
	public List<BugSearchResult> queryUserComments(String guid, String fconditions){
		fconditions = fconditions.trim();
		//TODO: Process filter conditions and add them
		int prod_id = Integer.parseInt(fconditions.substring(fconditions.indexOf("prod:")+"prod:".length()));
		
		String sql_prepd = sql_user_comments.replace("?", guid.toUpperCase()) + "and bhead.product_id="+prod_id;
		
		
		logger.info("Executing query: "+sql_prepd);
		return jdbcTemplate.query(sql_prepd, new BugSearchResultRowMapper<BugSearchResult>());
	}

}
