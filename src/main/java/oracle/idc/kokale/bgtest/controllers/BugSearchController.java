package oracle.idc.kokale.bgtest.controllers;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import oracle.idc.kokale.bgtest.beans.BugSearchResult;
import oracle.idc.kokale.bgtest.beans.BugSearchResultContainer;
import oracle.idc.kokale.bgtest.database.BugSearchDao;

@RestController
public class BugSearchController {
	private static final Logger logger = LoggerFactory.getLogger(BugSearchController.class);
	
	@Autowired
	BugSearchDao dao;

	@RequestMapping("/bugs/updatedby/{guid}")
	public BugSearchResultContainer updatedByUser(@PathVariable String guid){
		List<BugSearchResult> results = dao.queryUserComments(guid);
		logger.info("Found "+results.size()+" records for user:"+guid);
		return new BugSearchResultContainer(results);
	}
}
