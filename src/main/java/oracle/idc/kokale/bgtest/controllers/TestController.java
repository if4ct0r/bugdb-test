package oracle.idc.kokale.bgtest.controllers;

import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import oracle.idc.kokale.bgtest.beans.TestResponse;

@RestController
public class TestController {

	@RequestMapping("/test/{message}")
	public TestResponse test(@PathVariable String message) {
		return new TestResponse(message);
	}
}
