package oracle.idc.kokale.bgtest.beans;

public class BugSearchResult {
	
	int rptno;
	String subject;
	public int getRptno() {
		return rptno;
	}
	public void setRptno(int rptno) {
		this.rptno = rptno;
	}
	public String getSubject() {
		return subject;
	}
	public void setSubject(String subject) {
		this.subject = subject;
	}
	

}
