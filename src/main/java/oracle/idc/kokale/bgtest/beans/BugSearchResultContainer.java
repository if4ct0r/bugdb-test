package oracle.idc.kokale.bgtest.beans;

import java.util.List;

public class BugSearchResultContainer {

	List<BugSearchResult> bugSearchResults;

	public BugSearchResultContainer(List<BugSearchResult> bugSearchResults) {
		super();
		this.bugSearchResults = bugSearchResults;
	}

	public List<BugSearchResult> getBugSearchResults() {
		return bugSearchResults;
	}

	public void setBugSearchResults(List<BugSearchResult> bugSearchResults) {
		this.bugSearchResults = bugSearchResults;
	}
	
	
}
