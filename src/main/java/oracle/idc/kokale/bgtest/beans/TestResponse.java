package oracle.idc.kokale.bgtest.beans;

public class TestResponse {
	
	static int ID_CTR = 1;
	int id;
	String message = "Hello, ";
	
	public TestResponse(String message) {
		super();
		this.message = this.message + message;
		this.id = ID_CTR++;
	}
	
	public int getId() {
		return id;
	}
	
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = this.message + message;
	}

}
